# Money rounding error in Java - BigDecimal and JSR 354

**The project contains a few useful Gradle scripts. Check the `.gitlab.yml` for more details.**

| Branch | Pipeline                                                                                                                                                                                                       | Code coverage                                                                                                                                                                                                  | Test report (JaCoCo/Gradle)                                                                                                                                                                | Checkstyle reports (main/test)                                                                                                                                                                        | Dependency check report                                                                                   |
|--------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
| master | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/money-rounding-error-in-java/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/money-rounding-error-in-java/-/commits/master) | [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/money-rounding-error-in-java/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/money-rounding-error-in-java/-/commits/master) | [link](https://showmeyourcodeyoutube.gitlab.io/money-rounding-error-in-java/test-report) / [link](https://showmeyourcodeyoutube.gitlab.io/money-rounding-error-in-java/gradle-test-report) | [link](https://showmeyourcodeyoutube.gitlab.io/money-rounding-error-in-java/checkstyle/main.html) / [link](https://showmeyourcodeyoutube.gitlab.io/money-rounding-error-in-java/checkstyle/test.html) | [link](https://showmeyourcodeyoutube.gitlab.io/money-rounding-error-in-java/dependency-check-report.html) |

---

A simple Java project shows a decimal rounding problem.  
More details can be found here: https://dzone.com/articles/never-use-float-and-double-for-monetary-calculatio

📺 YouTube video: https://youtu.be/r5oOtlWpzHE

**What's JSR 354?**  

Java Specification Request (JSR) 354 provides an API for representing, transporting, and performing comprehensive calculations with Money and Currency.

**What is JavaMoney?**  

JavaMoney is the new monetary API for the Java™ Platform as well as related projects and libraries. Whereas the API (JSR 354) provides a portable and extensible API for handling of Money & Currency models, Moneta provides a production ready reference implementation.

JavaMoney: https://javamoney.github.io/

---

Rounding error is the difference between a rounded-off numerical value and the actual value.

![Rounding problem](docs/rounding-issue.png)

Example solutions:  

![Rounding solution](docs/solutions.png)

## Gradle

If you encounter any problems, run a Gradle command with additional parameters: `--warning-mode all` and `--stacktrace` (if you want to see the full output).

### Gradle commands (using wrapper)

- ./gradlew dependencies
- ./gradlew run
- ./gradlew clean
